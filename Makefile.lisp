(require 'asdf)
(asdf::oos 'asdf::compile-op "cl-ipmsg")

(require 'cl-ipmsg)

(vendor:eval-when-all
        (vendor:generate-exe (format nil "ipmsg.exe")
			     #'ipmsg::main))
