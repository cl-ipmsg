
(in-package cl-ipmsg)

(defconstant +IPMSG-VERSION+      #x0001)
(defconstant +IPMSG-DEFAULT-PORT+ #x0979)

(defclass ipmsg-protocol (protocol)
  ((version :initform +IPMSG-VERSION+ :reader protocol-ver)
   (ipif :initform nil :accessor protocol-if)
   (port :initform +IPMSG-DEFAULT-PORT+ :reader protocol-port)
   (thread      :initform nil :accessor protocol-thread)
   (packetindex :initform (random 1000) :accessor protocol-packet-index)
   (selfinfo :initform (make-instance 'ipmsg-protocol-selfinfo)
	     :accessor protocol-self-info)
   (hostinfo :initform () :accessor protocol-host-info)
   (buddy-list :initform nil :accessor protocol-buddy-list)

   (channel-list :initform nil :accessor protocol-channel-list)
   ))

(defmethod protocol-next-packet-index
    ((ipmsg-protocol ipmsg-protocol))
  (incf (protocol-packet-index ipmsg-protocol)))

(defun ipmsg-protocol-udp-callback (buffer)
  (declare (type (simple-array (unsigned-byte 8) *) buffer))
  (analyze-message (get-protocol-singleton 'ipmsg-protocol) buffer))

(defun ipmsg-send-initial-message ()
  (let ((protocol (get-protocol-singleton 'ipmsg-protocol)))
    (broadcast-command-message protocol
			       (protocol-port protocol)
			       :cmd :br-entry)))

(defun ipmsg-send-final-message ()
  (let ((protocol (get-protocol-singleton 'ipmsg-protocol)))
    (broadcast-command-message protocol
			       (protocol-port protocol)
			       :cmd :br-exit)))
  
(defun ipmsg-start-server (&optional port)
  (let ((protocol (get-protocol-singleton 'ipmsg-protocol)))
    (handler-case 
	(multiple-value-bind (thread socket)
	    (socket-server (format-ip (ip-interface-address (protocol-if protocol)))
			   (or port (protocol-port protocol))
			   #'ipmsg-protocol-udp-callback
			   nil :in-new-thread t :protocol :datagram)
	  (setf (protocol-thread protocol) thread
		(protocol-udp-socket protocol) socket)
	  (ipmsg-send-initial-message)
	  socket)
      (error ()
	      (format t "~&Error: program is running or an error occured.")
	      (vendor:quit))
      )))

(defun ipmsg-stop-server-and-quit ()
  (ipmsg-send-final-message)
  (vendor:quit)
  )

(defmethod broadcast-addr ((this ipmsg-protocol))
  (format-ip (ip-interface-broadcast-address (protocol-if this)))
  )

(defmethod analyze-message ((this ipmsg-protocol) buffer)
  (handler-case 
      (let ((analyzed-msg (analyze-ipmsg-message
			   (octets-to-string buffer :encoding :utf-8))))
	#+nil(warn "analyze-message: ~&  buffer=~a~&  msg=~a~&"
	      buffer analyzed-msg)
	(when (/= (getf analyzed-msg :ver) +IPMSG-VERSION+)
	  (format t "~&Warning: junk message or unsupported protocol version!~2%")
	  (return-from analyze-message nil))
	(apply #'call-command-handler
	       (list* this *remote-host* *remote-port* analyzed-msg)))
    (invalid-utf8-starter-byte () nil)
    (invalid-utf8-continuation-byte () nil)))
    

(defmethod make-message ((this ipmsg-protocol) &rest arguments)
  #+nil(warn "make-message: arguments=~a" arguments)
  (string-to-octets (apply #'make-ipmsg-message arguments) :encoding :utf-8))

(defmacro define-ipmsg-command-recipient
    (name-and-aliases value param-list &body body)
  `(define-protocol-command-recipient ipmsg-protocol ,name-and-aliases
     ,value ,param-list
     #+nil(progn (format t "~& DBG:ipmsg: cmd = ~a , msg = ~a ~%" cmd msg)
		 (force-output))
     #+nil(warn "in")
     (format t "")
     ,@body
     (format t "")     
     #+nil(warn "out")))

(defun analyze-ipmsg-message (string)
  (let ((string-array (usocket::split-sequence #\: string)))
    (list* :ver      (parse-integer (first string-array) :junk-allowed t)
	   :packno   (parse-integer (second string-array):junk-allowed t)
	   :username (third  string-array)
	   :hostname (fourth string-array)
	   :cmd      (logand (parse-integer (fifth  string-array) :junk-allowed t) #xFF)
	   :cmd-opt  (logand (parse-integer (fifth  string-array) :junk-allowed t)
			     #xFFFFFF00)
	   (let ((msg-array (usocket::split-sequence #\Nul (sixth string-array))))
	     (if (> (length msg-array) 1)
		 (list :msg (first msg-array)
		       :exmsg (format nil #.(format nil "~~{~~a~~^~a~~}" #\Nul)
				      (rest msg-array)))
		 (list :msg (first msg-array)))))))
 
(defun make-ipmsg-message (&key cmd cmd-opt msg exmsg &allow-other-keys)
  (let ((protocol (get-protocol-singleton 'ipmsg-protocol)))
    (format nil "~a:~a:~a:~a:~a:~a"
	    (protocol-ver protocol)
	    (protocol-next-packet-index protocol)
	    (user-name (protocol-self-info protocol))
	    (host-name (protocol-self-info protocol))
	    (logior (gethash cmd (cmd-table protocol))
		    (or (and cmd-opt
			     (gethash cmd-opt (cmd-table protocol)))
			0))
	    (if exmsg 
		(format nil "~a~a~a" (or msg "") #\Nul exmsg)
		(or msg "")))))

(defun register-host (&rest user-info)
  #+nil(warn "in")
  (unregister-host (getf user-info :username)
		   (getf user-info :hostname))
  (let ((protocol (get-protocol-singleton 'ipmsg-protocol)))
    (push user-info (protocol-buddy-list protocol)))
  #+nil(warn "out")
  )

(defun unregister-host (user-name host-name)
  #+nil(warn "in")
  (let ((protocol (get-protocol-singleton 'ipmsg-protocol)))
    (setf (protocol-buddy-list protocol)
	  (remove-if (lambda (item)
		       (and (string-equal (getf item :username) user-name)
			    (string-equal (getf item :hostname) host-name)))
		     (protocol-buddy-list protocol))))
  #+nil(warn "out")
  )

(defun get-compound-command-index (&rest command-list)
  (let ((cmd-table (cmd-table (get-protocol-singleton 'ipmsg-protocol))))
    (apply #'logior (map 'list (lambda (cmd) (gethash cmd cmd-table)) command-list))))


;;  option for all command
(defconstant IPMSG_ABSENCEOPT                #x00000100)
(defconstant IPMSG_SERVEROPT                 #x00000200)
(defconstant IPMSG_DIALUPOPT                 #x00010000)
(defconstant IPMSG_FILEATTACHOPT             #x00200000)
(defconstant IPMSG_ENCRYPTOPT                #x00400000)

;;  option for send command  
(defconstant IPMSG_SENDCHECKOPT              #x00000100)
(defconstant IPMSG_SECRETOPT                 #x00000200)
(defconstant IPMSG_BROADCASTOPT              #x00000400)
(defconstant IPMSG_MULTICASTOPT              #x00000800)
(defconstant IPMSG_NOPOPUPOPT                #x00001000)
(defconstant IPMSG_AUTORETOPT                #x00002000)
(defconstant IPMSG_RETRYOPT                  #x00004000)
(defconstant IPMSG_PASSWORDOPT               #x00008000)
(defconstant IPMSG_NOLOGOPT                  #x00020000)
(defconstant IPMSG_NEWMUTIOPT                #x00040000)
(defconstant IPMSG_NOADDLISTOPT              #x00080000)
(defconstant IPMSG_READCHECKOPT              #x00100000)

(defconstant IPMSG_SECRETEXOPT               (logior IPMSG_READCHECKOPT
						       IPMSG_SECRETOPT))

;; encryption flags for encrypt command
(defconstant IPMSG_RSA-512                   #x00000001)
(defconstant IPMSG_RSA-1024                  #x00000002)
(defconstant IPMSG_RSA-2048                  #x00000004)
(defconstant IPMSG_RC2-40                    #x00001000)
(defconstant IPMSG_RC2-128                   #x00004000)
(defconstant IPMSG_RC2-256                   #x00008000)
(defconstant IPMSG_BLOWFISH-128              #x00020000)
(defconstant IPMSG_BLOWFISH-256              #x00040000)
(defconstant IPMSG_SIGN-MD5                  #x10000000)

;; compatibilty for Win beta version 
(defconstant IPMSG_RC2-40OLD                 #x00000010)    ;; for beta1-4 only

(defconstant IPMSG_RC2-128OLD                #x00000040)    ;; for beta1-4 only

(defconstant IPMSG_BLOWFISH-128OLD                 #x00000400)    ;; for beta1-4 only

(defconstant IPMSG_RC2-40ALL                 (logior IPMSG_RC2-40
						       IPMSG_RC2-40OLD))

(defconstant IPMSG_RC2-128ALL                (logior IPMSG_RC2-128
						       IPMSG_RC2-128OLD))

(defconstant IPMSG_BLOWFISH-128ALL           (logior IPMSG_BLOWFISH-128
						       IPMSG_BLOWFISH-128OLD))


;; file types for fileattach command
(defconstant IPMSG_FILE-REGULAR              #x00000001)
(defconstant IPMSG_FILE-DIR                  #x00000002)
(defconstant IPMSG_FILE-RETPARENT            #x00000003)    ;; return parent directory

(defconstant IPMSG_FILE-SYMLINK              #x00000004)
(defconstant IPMSG_FILE-CDEV                 #x00000005)    ;; for UNIX

(defconstant IPMSG_FILE-BDEV                 #x00000006)    ;; for UNIX

(defconstant IPMSG_FILE-FIFO                 #x00000007)    ;; for UNIX

(defconstant IPMSG_FILE-RESFORK              #x00000010)    ;; for Mac


;; file attribute options for fileattach command 
(defconstant IPMSG_FILE-RONLYOPT             #x00000100)
(defconstant IPMSG_FILE-HIDDENOPT            #x00001000)
(defconstant IPMSG_FILE-EXHIDDENOPT          #x00002000)    ;; for MacOS X

(defconstant IPMSG_FILE-ARCHIVEOPT           #x00004000)
(defconstant IPMSG_FILE-SYSTEMOPT            #x00008000)

;; extend attribute types for fileattach command
(defconstant IPMSG_FILE-UID                  #x00000001)
(defconstant IPMSG_FILE-USERNAME             #x00000002)    ;; uid by string

(defconstant IPMSG_FILE-GID                  #x00000003)
(defconstant IPMSG_FILE-GROUPNAME            #x00000004)    ;; gid by string

(defconstant IPMSG_FILE-PERM                 #x00000010)    ;; for UNIX

(defconstant IPMSG_FILE-MAJORNO              #x00000011)    ;; for UNIX devfile

(defconstant IPMSG_FILE-MINORNO              #x00000012)    ;; for UNIX devfile

(defconstant IPMSG_FILE-CTIME                #x00000013)    ;; for UNIX

(defconstant IPMSG_FILE-MTIME                #x00000014)
(defconstant IPMSG_FILE-ATIME                #x00000015)
(defconstant IPMSG_FILE-CREATETIME           #x00000016)
(defconstant IPMSG_FILE-CREATOR              #x00000020)    ;; for Mac

(defconstant IPMSG_FILE-FILETYPE             #x00000021)    ;; for Mac

(defconstant IPMSG_FILE-FINDERINFO           #x00000022)    ;; for Mac

(defconstant IPMSG_FILE-ACL                  #x00000030)
(defconstant IPMSG_FILE-ALIASFNAME           #x00000040)    ;; alias fname

(defconstant IPMSG_FILE-UNICODEFNAME         #x00000041)    ;; UNICODE fname

(define-ipmsg-command-recipient (:NOOPERATION :NOP) #x00000000
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)

(define-ipmsg-command-recipient :BR-ENTRY                  #x00000001 
  (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	&allow-other-keys)
  #+nil(warn "in")
  (let ((protocol (get-protocol-singleton 'ipmsg-protocol)))
    (register-host :username username :username username :hostname hostname
		   :nickname msg :groupname exmsg :status cmd-opt
		   :host hostaddr :port port)
    (send-command-message (get-protocol-singleton 'ipmsg-protocol)
			  hostaddr port
			  :cmd :ANSENTRY
			  :msg (user-name (protocol-self-info protocol))))
  #+nil(warn "out")
  )
(define-ipmsg-command-recipient :BR-EXIT                   #x00000002 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
    (unregister-host username hostname)
    )
(define-ipmsg-command-recipient :ANSENTRY                  #x00000003 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
    #+nil(warn "in")
    (register-host :username username :hostname hostname
		   :nickname msg :groupname exmsg :status cmd-opt
		   :host hostaddr :port port)
    #+nil(warn "out")
  )
(define-ipmsg-command-recipient :BR-ABSENCE                #x00000004 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)

(define-ipmsg-command-recipient :BR-ISGETLIST              #x00000010 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :OKGETLIST                 #x00000011 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :GETLIST                   #x00000012 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :ANSLIST                   #x00000013 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :BR-ISGETLIST2             #x00000018 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)

(define-ipmsg-command-recipient :SENDMSG                   #x00000020 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
    (when (= (logand cmd-opt IPMSG_SENDCHECKOPT) IPMSG_SENDCHECKOPT)
      (let ((protocol (get-protocol-singleton 'ipmsg-protocol)))
	(send-command-message (get-protocol-singleton 'ipmsg-protocol)
			      hostaddr port
			      :cmd :RECVMSG
			      :msg (format nil "~a" packno))))
    (format t "~%~a:~a~&# " username msg)(force-output)
    )

(define-ipmsg-command-recipient :RECVMSG                   #x00000021 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  )

(define-ipmsg-command-recipient :READMSG                   #x00000030 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :DELMSG                    #x00000031 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :ANSREADMSG                #x00000032 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)

(define-ipmsg-command-recipient :GETINFO                   #x00000040 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :SENDINFO                  #x00000041 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)

(define-ipmsg-command-recipient :GETABSENCEINFO            #x00000050 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :SENDABSENCEINFO           #x00000051 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)

(define-ipmsg-command-recipient :GETFILEDATA               #x00000060 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :RELEASEFILES              #x00000061 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :GETDIRFILES               #x00000062 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)

(define-ipmsg-command-recipient :GETPUBKEY                 #x00000072 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)
(define-ipmsg-command-recipient :ANSPUBKEY                 #x00000073 
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  nil)

(define-ipmsg-command-recipient :FEIQ-UNKNOWNMSG                 #x00000079
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
  #+nil(format t "~&FEIQ-SEND [~a] ~a ~a~%" username msg exmsg)
  )


(define-ipmsg-command-recipient :EXT-SENDCHANNELMSG               #x00000090
    (&key hostaddr port ver packno username hostname cmd cmd-opt msg exmsg
	  &allow-other-keys)
    (let ((protocol (get-protocol-singleton 'ipmsg-protocol)))
      #+nil(warn "~a ~a ~a" exmsg (protocol-channel-list protocol) msg)
      (when (member exmsg
		    (protocol-channel-list protocol) :test #'string-equal)
;;	(when (not (and (equal hostaddr
;;			       (format-ip (ip-interface-address
;;					   (protocol-if protocol))))
;;			(equal port
;;			       (protocol-port protocol))))
	    (format t "~&[~a]~a:~a~%" exmsg username msg)(force-output))))
;;)

