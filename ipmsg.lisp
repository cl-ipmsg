
(in-package cl-ipmsg)

(defun msgloop ()
  (loop
     (format t "~&> ")(force-output)
     (let* ((command-line (read-line)))
       (when (string/= (subseq command-line (max (1- (length command-line)) 0)) "\\")
	 (with-input-from-string (s command-line)
	   (let* ((phrase1 (format nil "~a" (read s nil nil nil)))
		  (phrase2 (read s nil nil nil))
		  (phrase3 (read s nil nil nil))
 		  (phrase4 (read s nil nil nil)))
	     (cond
	       ((string-equal phrase1 "quit") (return-from msgloop nil))
	       ((string-equal phrase1 "list")
		(progn
		  (format t "~2&Here is the list of your buddies online:~&")
		  (loop for i from 0
		     below (length (protocol-buddy-list
				    (get-protocol-singleton 'ipmsg-protocol)))
		     do (format t "~&~a: ~a~&"
				(1+ i)
				(nth i (protocol-buddy-list
					(get-protocol-singleton 'ipmsg-protocol)))))))
	       ((string-equal phrase1 "send-ip")
		(send-command-message (get-protocol-singleton 'ipmsg-protocol)
				      phrase2
				      (or phrase4
					  (protocol-port
					      (get-protocol-singleton 'ipmsg-protocol)))
				      :cmd :sendmsg
				      :msg (format nil "~a" phrase3)))
	       ((string-equal phrase1 "send")
		(let* ((nickname-to-find (format nil "~a" phrase2))
		       (target-buddy
			(find-if (lambda (item)
				   (string-equal (getf item :username nil)
						 nickname-to-find))
				 (protocol-buddy-list
				  (get-protocol-singleton 'ipmsg-protocol)))))
		  (if (null target-buddy)
		      (format t "~& Error: no such buddy.~%")
		      (send-command-message (get-protocol-singleton 'ipmsg-protocol)
					    (format-ip (getf target-buddy :host))
					    (getf target-buddy :port)
					    :cmd :sendmsg
					    :msg (format nil "~a" phrase3)))))
	       ((string-equal phrase1 "csend")
		(let* ((channel-to-find (format nil "~a" phrase2)))
		  (if (not
		       (member channel-to-find
			       (protocol-channel-list (get-protocol-singleton
						       'ipmsg-protocol))
			       :test #'string-equal))
		      (format t "~& Error: no such channel.~%")
		      (broadcast-command-message (get-protocol-singleton 'ipmsg-protocol)
					(protocol-port (get-protocol-singleton
						'ipmsg-protocol))
					:cmd :ext-sendchannelmsg
					:msg (format nil "~a" phrase3)
			        	:exmsg (format nil "~a" phrase2)))))
	       ((string-equal phrase1 "add-channel")
		(pushnew (format nil "~a" phrase2)
			 (protocol-channel-list (get-protocol-singleton 'ipmsg-protocol))
			 :test 'equal))
	       ((string-equal phrase1 "del-channel")
		(let* ((channel-to-find (format nil "~a" phrase2)))
		  (if (not
		       (member channel-to-find
			       (protocol-channel-list (get-protocol-singleton
						       'ipmsg-protocol)) :test #'equal))
		      (format t "~& Error: no such channel.~%")		      
		      (setf (protocol-channel-list (get-protocol-singleton 'ipmsg-protocol))
			    (remove-if (lambda (item) (equal item channel-to-find))
				       (protocol-channel-list (get-protocol-singleton
							       'ipmsg-protocol)))))))
	       ((or (string-equal phrase1 "help") (string-equal phrase1 "?"))
		(format t "~& Following commands are avaiable:~%~& send~%~& send-ip~%~& list~%~& quit~%~& add-channel~%~& del-channel~%~& csend~2%"))
	       (t (format t "~&  Unknown command!~%")))))))))

(defun main ()
  (showlogo)
  (let ((self-info (make-instance 'ipmsg-protocol-selfinfo)))
    (setf (user-name self-info) (init-username)
	  (host-name self-info) (usocket::get-host-name))
    (setf (protocol-if (get-protocol-singleton 'ipmsg-protocol))
	  (init-user-ip-interface)))
  (ipmsg-start-server)
  (msgloop)
  (vendor:quit)
  )

