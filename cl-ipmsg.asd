
(defpackage cl-ipmsg-asd
  (:use :cl :asdf))

(in-package cl-ipmsg-asd)

(defsystem cl-ipmsg
  :name "cl-ipmsg"
  :author "Charles Lew(crlf0710@gmail.com)"
  :maintainer "Charles Lew(crlf0710@gmail.com)"
  :license "MIT"
  :description "An implementation for IPMSG protocol"
  :depends-on (:usocket :babel :ip-interfaces :cl-vendor)
  :components ((:file "package")
	       (:file "protocol" :depends-on ("package"))
	       (:file "ipmsg-userinfo" :depends-on ("package"))
	       (:file "ipmsg-protocol" :depends-on ("protocol" "ipmsg-userinfo"))
	       (:file "ipmsg" :depends-on ("package" "ipmsg-protocol"))
	       ))
