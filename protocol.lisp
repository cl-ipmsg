
(in-package cl-ipmsg)

(defclass protocol ()
  ((udp-socket  :initform nil :accessor protocol-udp-socket)
   (command-to-ncommand-table :initform (make-hash-table :test #'eq)
			     :accessor cmd-table)
   (ncommand-table :initform (make-hash-table :test #'eql)
		  :accessor ncmd-table))
  )

(defmethod call-command-handler ((this protocol) srcaddr srcport &rest arguments)
  (let* ((command (getf arguments :cmd))
	 (handler (gethash command (ncmd-table this))))
    (if (not handler)
	(format t "~&Warning: Try to execute unknown command: ~a~&arguments=~a~%"
		command arguments))
    (when handler
      (apply handler (list* :hostaddr srcaddr :port srcport arguments)))))

(defgeneric broadcast-addr (protocol))
(defgeneric analyze-message (protocol buffer))
(defgeneric make-message (protocol &rest arguments))

(defmethod  send-command-message ((this protocol) dest port &rest arguments)
  (let* ((buffer (apply #'make-message (list* this arguments)))
	 (buffer-size (length buffer)))
    (when (not (protocol-udp-socket this))
      (format t "~2&  Error: socket not initialized!~2%")
      (return-from send-command-message nil))
    #+nil(warn "DBG: ~a~&~a~&~a" port (octets-to-string buffer) buffer-size)
    (socket-send (protocol-udp-socket this) buffer buffer-size :port port :host dest)))

(defmethod  broadcast-command-message ((this protocol) port &rest arguments)
  #+sbcl (progn (setf (sb-bsd-sockets::sockopt-broadcast
		       (usocket::socket (protocol-udp-socket this))) t)
		(apply #'send-command-message
		       (list* this (broadcast-addr this) port arguments))
		(setf (sb-bsd-sockets::sockopt-broadcast
		       (usocket::socket (protocol-udp-socket this))) nil))
  )

(defmacro define-protocol-command-recipient
    (protocol name-and-aliases value param-list &body body)
  (let ((method-fn (gensym))
	(protocol-object (gensym)))
    `(eval-when (:load-toplevel :execute)
       (let ((,method-fn (lambda ,param-list ,@body))
	     (,protocol-object (get-protocol-singleton ',protocol)))
	 ,@(loop for cmd-name in (if (listp name-and-aliases) name-and-aliases (list name-and-aliases))
	      collecting `(setf (gethash ,cmd-name (cmd-table ,protocol-object)) ,value))
	 (setf (gethash ,value (ncmd-table ,protocol-object)) ,method-fn)))))

(defvar *protocol-singleton-table* (make-hash-table :test #'eq))

(defun get-protocol-singleton (class-name)
  (or (gethash class-name *protocol-singleton-table*)
      (setf (gethash class-name *protocol-singleton-table*)
	    (make-instance class-name))))
