(require 'usocket)
(require 'babel)
(require 'ip-interfaces)

(defpackage cl-ipmsg
  (:nicknames ipmsg)
  (:use #:cl #:usocket #:babel #:ip-interfaces)
  )

(in-package cl-ipmsg)

(defun showlogo ()
  (format t "~2%  cl-ipmsg 0.01 by CrLF0710~2&")
  )

(defclass ipmsg-application ()
  ((dummy :allocation :class :initform (showlogo))))
