
(in-package cl-ipmsg)

(defun init-username ()
  (format t "~&Please input your Name:")(force-output)
  (read-line))

(defun init-user-ip-interface ()
  (format t "~&You have the following network cards. ")
  (let ((ip-interfaces (reverse (get-ip-interfaces))))
    (loop for i from 1 to (length ip-interfaces)
       do (format t "~& ~a: ~a~%" i (ip-interface-address
				     (nth (1- i) ip-interfaces))))
    (loop
       (format t "~&Choose one[Enter for first]:")(force-output)
       (let ((curchoice (read-line *standard-input* nil "")))
	 (if (string= curchoice "")
	     (return (first ip-interfaces))
	     (let ((curindex (parse-integer curchoice :junk-allowed t)))
	       (when (and (integerp curindex)
			  (>= curindex 1)
			  (<= curindex (length ip-interfaces)))
		 (return (nth (1- curindex) ip-interfaces)))))))))

(defun format-ip (vector)
  (format nil "~{~a~^.~}" (coerce vector 'list)))

(defclass ipmsg-protocol-selfinfo ()
  ((username :accessor user-name :allocation :class :initform nil)
   (hostname :accessor host-name :allocation :class :initform nil)
   (groupname :accessor group-name :allocation :class)
   )
  )
